import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    favorites: JSON.parse(localStorage.getItem("favorite-books")) || [],
    books: JSON.parse(localStorage.getItem("list-books")) || [],
  },
  mutations: {
    ADD_TO_FAVORITE(state, book) {
      state.favorites.push(book);
      localStorage.setItem("favorite-books", JSON.stringify(state.favorites));
    },
    REMOVE_FROM_FAVORITE(state, book) {
      const index = state.favorites.findIndex((f) => f.id === book.id);
      state.favorites.splice(index, 1);
      localStorage.setItem("favorite-books", JSON.stringify(state.favorites));
    },
    SET_LIST_BOOKS(state, book) {
      state.books = book;
      localStorage.setItem("list-books", JSON.stringify(state.books));
    },
  },
  actions: {
    addToFavorite({ commit }, book) {
      commit("ADD_TO_FAVORITE", book);
    },
    removeFromFavorite({ commit }, book) {
      commit("REMOVE_FROM_FAVORITE", book);
    },
    setListBooks({ commit }, book) {
      commit("SET_LIST_BOOKS", book);
    },
  },
  getters: {
    favoritesCount: (state) => state.favorites.length,
    isBookInFavorites: (state) => (book) => {
      return state.favorites.some(
        (favoriteBook) => favoriteBook.id === book.id
      );
    },
  },
});

export default store;
