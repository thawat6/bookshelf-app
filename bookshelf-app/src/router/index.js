import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "@/components/Layout";
import Shelf from "@/components/Shelf";
import Book from "@/components/Book";
import Favorite from "@/components/Favorite";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: Layout,
    children: [
      {
        path: "/",
        component: Shelf,
        name: "Shelf",
      },
      {
        path: "/Book",
        component: Book,
        name: "Book",
      },
      {
        path: "/Favorite",
        component: Favorite,
        name: "Favorite",
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
